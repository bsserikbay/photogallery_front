import React from "react";
import PhotoListItem from "./PhotoListItem/PhotoListItem";

const PhotoList = props => {
  return (
    <div style={{width: '100%', padding: 20, display: 'flex'}}>
      {
        props.photos.map(photo => {
          return <PhotoListItem
            id={photo._id}
            key={photo._id}
            title={photo.title}
            image={photo.image}
            user={photo.user}
            button={props.button}
          />;
        })
      }
    </div>
  );
};

export default PhotoList;
