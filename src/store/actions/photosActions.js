import axios from "../../axios-api";
import {
    CREATE_PHOTO_SUCCESS,
    FETCH_FULL_PHOTO_SUCCESS,
    FETCH_PHOTOS_ERROR,
    FETCH_PHOTOS_SUCCESS,
    FETCH_REQUEST_SUCCESS
} from "../actionTypes";
import {NotificationManager} from "react-notifications";

const fetchRequestSuccess = () => {
    return {type: FETCH_REQUEST_SUCCESS}
};
const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos};
};
const fetchFullPhotoSuccess = photo => {
    return {type: FETCH_FULL_PHOTO_SUCCESS, photo};
};

const fetchPhotosError = error => {
    return {type: FETCH_PHOTOS_ERROR, error};
};


export const fetchPhotos = (id) => {
    if (id) {
        return dispatch => {
            return axios.get(`/photos?user=${id}`).then(response => {
                dispatch(fetchPhotosSuccess(response.data));
            });
        };
    } else {
        return dispatch => {
            return axios.get("/photos").then(response => {
                dispatch(fetchPhotosSuccess(response.data));
            });
        };
    }
};

export const fetchFullView = (id) => {
    return dispatch => {
        return axios.get(`/photos/${id}`).then(response => {
            dispatch(fetchFullPhotoSuccess(response.data));
        });
    };
};

const createPhotoSuccess = () => {
    return {type: CREATE_PHOTO_SUCCESS};
};

export const createPhoto = product => {
    return dispatch => {
        return axios.post(
            "/photos",
            product
        ).then(() => {
            dispatch(createPhotoSuccess());
            NotificationManager.success("Photo Posted");

        });

    };
};


export const deletePhoto = (id) => {
    return dispatch => {
        return axios.delete(
            `/photos/${id}`,
        ).then(() => {
            dispatch(fetchRequestSuccess())
            window.location.reload(false);
            NotificationManager.success("Photo deleted");
        }, error => {
            dispatch(fetchPhotosError(error))
        })
    };
};

