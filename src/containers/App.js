import React, {Component} from 'react';
import Toolbar from "../components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {withRouter} from "react-router-dom";
import {NotificationContainer} from "react-notifications"
import {connect} from "react-redux";
import {logoutUser} from "../store/actions/usersActions";
import Routes from "../Routes";

const mapStateToProps = state => {
  return {
    user: state.users.user
  };
};

class App extends Component {
  render() {
    return (
      <>
        <NotificationContainer />
        <header className="mb-3">
          <Toolbar
            logout={this.props.logout}
            user={this.props.user}
          />
        </header>
        <main>
          <Container>
            <Routes user={this.props.user} />
          </Container>
        </main>
      </>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logoutUser())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
