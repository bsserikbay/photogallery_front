import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from "connected-react-router";
import axios from "./axios-api";
import * as serviceWorker from './serviceWorker';

import 'bootstrap/dist/css/bootstrap.min.css';
import "react-notifications/lib/notifications.css";
import App from './containers/App';
import store, {history} from "./store/configureStore";

axios.interceptors.request.use(config => {
  config.headers['Token'] = store.getState().users.user && store.getState().users.user.token;
  return config;
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
