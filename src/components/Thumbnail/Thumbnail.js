import React, {Component} from "react";
import config from "../../config";
import defaultImage from "../../assets/images/default.jpg";
import {fetchFullView} from "../../store/actions/photosActions";
import {connect} from "react-redux";
import FullView from "../UI/Modal/FullView";


class Thumbnail extends Component {

    state = {
        show: false
    }


    showFullPhoto = (e, id) => {
        e.preventDefault()
        this.setState({show: true})
        this.props.onFetchFullView(id)
    };

    showModal = () => {
        this.setState(prev => ({
            show: !prev.show
        }));
    };

    render() {
        let image = defaultImage;
        if (this.props.image) {
            image = config.apiURL + "/uploads/" + this.props.image;
        }


        return (
            <>
                <FullView
                    onClose={this.showModal}
                    show={this.state.show}
                    image={this.props.fullPhoto.image}
                    size="lg"
                    backdrop={true}
                    autoFocus={true}
                />

                <img src={image}
                     alt={this.props.title}
                     style={{
                         float: "left",
                         width: "100px",
                         height: "auto"
                     }}
                     onClick={(e) => this.showFullPhoto(e, this.props.id)}
                />
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        fullPhoto: state.photos.fullPhoto,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchFullView: (id) => dispatch(fetchFullView(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Thumbnail);
