import axios from "../../axios-api";
import {push} from "connected-react-router"
import {NotificationManager} from "react-notifications";
import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "../actionTypes";

const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS}
};
const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = (userData) => {
  return dispatch => {
    return axios.post("/users", userData).then(() => {
      dispatch(registerUserSuccess());
      dispatch(push("/"));
    }, error => {
      if (error.response && error.response.data) {
        dispatch(registerUserFailure(error.response.data));
      } else {
        dispatch(registerUserFailure({global: "No internet"}));
      }
    });
  }
}

const loginUserSuccess = user => {
  return {type: LOGIN_USER_SUCCESS, user};
};
const loginUserFailure = error => {
  return {type: LOGIN_USER_FAILURE, error};
};
export const loginUser = userData => {
  return dispatch => {
    return axios.post("/users/sessions", userData).then(response => {
      dispatch(loginUserSuccess(response.data));
      dispatch(push("/"));
      NotificationManager.success("Hello, " + response.data.username);
    }, error => {
      dispatch(loginUserFailure(error.response.data));
      NotificationManager.error("Something went wrong");
    });
  };
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {Token: token};

    axios.delete("/users/sessions", {headers}).then(() => {
      dispatch({type: LOGOUT_USER});
      dispatch(push("/"));
      NotificationManager.success("Logout success");
    });
  };
};

export const facebookLogin = data => {
  return dispatch => {
    axios.post("/users/facebookLogin", data).then(response => {
      dispatch(loginUserSuccess(response.data))
      dispatch(push("/"));
      NotificationManager.success("Logged in with Facebook");
    }, error => {
      dispatch(loginUserFailure(error.response.data));
    })
  };
};