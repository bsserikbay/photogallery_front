import React, {Component} from "react";
import {fetchPhotos} from "../../store/actions/photosActions";
import {connect} from "react-redux";
import PhotoList from "../../components/PhotoList/PhotoList";

class Gallery extends Component {

    componentDidMount() {
        this.props.onFetchPhotos();
    }

    render() {
        return (
            <>

                <PhotoList
                    photos={this.props.photos}
                />
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        photos: state.photos.photos,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPhotos: () => dispatch(fetchPhotos())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
