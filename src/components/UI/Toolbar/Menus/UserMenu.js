import React from "react";
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterLink} from "react-router-dom";

const UserMenu = ({user, logout}) => {
  return (
    <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav caret>
        Hello, {user.displayName || user.username}
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem tag={RouterLink} to={`gallery/${user._id}`}   >
          Profile
        </DropdownItem>
        <DropdownItem divider />
        <DropdownItem onClick={logout}>
          Logout
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default UserMenu;
