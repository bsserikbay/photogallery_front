import React from "react";
import {NavItem, NavLink} from "reactstrap";
import {NavLink as RouterLink} from "react-router-dom";

const AnonymousMenu = () => {
  return (
    <>
      <NavItem>
        <NavLink tag={RouterLink} to="/registration" exact>Sign up</NavLink>
      </NavItem>
      <NavItem>
        <NavLink tag={RouterLink} to="/login" exact>Sign in</NavLink>
      </NavItem>
    </>
  );
};

export default AnonymousMenu;