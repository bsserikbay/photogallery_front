import {FETCH_PHOTOS_SUCCESS, FETCH_FULL_PHOTO_SUCCESS, FETCH_PHOTOS_ERROR, FETCH_REQUEST_SUCCESS} from "../actionTypes";

const initialState = {
  photos: [],
  fullPhoto: '',
  errors: "",
};

const photoReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_REQUEST_SUCCESS:
      return {...state}
    case FETCH_PHOTOS_SUCCESS:
      return {...state, photos: action.photos};
    case FETCH_FULL_PHOTO_SUCCESS:
      return {...state, fullPhoto: action.photo};
    case FETCH_PHOTOS_ERROR:
      return {...state, errors: action.errors};
    default:
      return state;
  }
};

export default photoReducer;
