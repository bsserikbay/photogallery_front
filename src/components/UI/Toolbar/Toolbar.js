import React from "react";
import {NavLink as RouterLink} from "react-router-dom";
import {
  Container,
  Nav,
  Navbar,
  NavbarBrand,
} from "reactstrap";
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";

const Toolbar = props => {
  return (
    <Navbar color="info" light expand="md">
      <Container >
        <NavbarBrand tag={RouterLink} to="/" exact className="text-white">
          Photo Gallery
        </NavbarBrand>
        <Nav className="ml-auto" navbar>
          {props.user ?
            <UserMenu
              user={props.user}
              logout={props.logout}
            /> : <AnonymousMenu />}
        </Nav>
      </Container>
    </Navbar>
  );
};

export default Toolbar;
