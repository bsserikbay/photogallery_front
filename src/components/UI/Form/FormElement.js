import React from "react";
import PropTypes from "prop-types";
import {Col, FormFeedback, FormGroup, Input, Label} from "reactstrap";

const FormElement = props => {
  let formControlChildren;
  if (props.type === "select" && props.options) {
    formControlChildren = props.options.map(option => {
      return <option
        value={option._id}
        key={option._id}
      >
        {option.title}
      </option>
    });
    formControlChildren.unshift(
      <option
        value=""
        key={1}
      >
        Select {props.title}
      </option>
    );
  }
  return (
    <FormGroup row>
      <Label sm={2} for={props.name}>{props.title}</Label>
      <Col sm={10}>
        <Input
          type={props.type}
          id={props.name}
          name={props.name}
          placeholder={props.placeholder}
          required={props.required}
          value={props.value}
          invalid={!!props.error}
          onChange={props.onChange}
        >
          {formControlChildren}
        </Input>
        {
          props.error && <FormFeedback>
            {props.error}
          </FormFeedback>
        }
      </Col>
    </FormGroup>
  );
};

FormElement.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  required: PropTypes.bool,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  error: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object),
  onChange: PropTypes.func.isRequired
};

export default FormElement;