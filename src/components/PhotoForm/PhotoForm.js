import React, {Component} from "react";
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

class PhotoForm extends Component {
  state = {
    title: "",
    image: "",
  };

  onChangeInputHandler = e => {
    this.setState({[e.target.name]: e.target.value});
  };
  onFileChangeHandler = e => {
    this.setState({image: e.target.files[0]});
  };

  onSubmitForm = e => {
    e.preventDefault();
    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });
    this.props.onSubmit(formData);
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors &&
        this.props.error.errors[fieldName] &&
        this.props.error.errors[fieldName].properties &&
        this.props.error.errors[fieldName].properties.message;
  };

  render() {
    return (
      <Form onSubmit={this.onSubmitForm}>
        <FormElement
          type="text"
          title="Title"
          value={this.state.title}
          name="title"
          required={true}
          placeholder="Product title"
          onChange={this.onChangeInputHandler}
          error={this.getFieldError("title")}
        />
        <FormElement
          type="file"
          title="Image"
          name="image"
          onChange={this.onFileChangeHandler}
          error={this.getFieldError("image")}
          required={true}
        />
        <FormGroup row>
          <Col sm={{offset: 2, size: 10}}>
            <Button type="submit" color="primary">
              Save
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}



export default PhotoForm;
