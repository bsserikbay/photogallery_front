import React, {Component} from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import Thumbnail from "../../Thumbnail/Thumbnail"
import {deletePhoto} from "../../../store/actions/photosActions";
import {connect} from "react-redux";

class PhotoListItem extends Component {

    deleteHandler = (e, id) => {
        e.preventDefault();
        this.props.onDeletePhoto(id)
    };


    render() {

        return (
            <div key={this.props.id}
                 style={{width: 120, border: '1px solid #eeeeee', borderRadius: 3, padding: 10, margin: 5}}>
                <div
                    style={{width: '100%', display: 'flex'}}
                >
                    {this.props.button ? <span
                        role="img"
                        aria-label="Close"
                        style={{marginLeft: 'auto', fontSize: 12, cursor: 'pointer'}}
                        onClick={(e)=> this.deleteHandler(e,  this.props.id)}
                    >
          ❌
        </span> : null}
                </div>
                <div>
                    <h6
                    style={{fontSize: 12}}
                    >{this.props.title}</h6>
                    <Thumbnail
                        image={this.props.image}
                        title={this.props.title}
                        id={this.props.id}

                    />
                    <Link to={`/gallery/${this.props.user._id}`}>
                        {this.props.user.username}
                    </Link>
                </div>
            </div>
        );
    }
}


PhotoListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,

};


const mapDispatchToProps = dispatch => {
    return {
        onDeletePhoto: (id) => dispatch(deletePhoto(id))
    };
};

export default connect(null, mapDispatchToProps)(PhotoListItem);
