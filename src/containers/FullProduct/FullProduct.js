import React, {Component} from "react";
import axios from "../../axios-api";
import ProductThumbnail from "../../components/ProductThumbnail/Thumbnail";

class FullProduct extends Component {
  state = {
    product: {}
  };

  componentDidMount() {
    const id = this.props.match.params.id;
    axios.get("/products/" + id).then(response => {
      this.setState({product: response.data});
    });
  }
  render() {
    return (
      <>
        <h1>{this.state.product.title} | <small>{this.state.product.price} USD</small></h1>
        <ProductThumbnail
          title={this.state.product.title}
          image={this.state.product.image}
        />
        <p>{this.state.product.description}</p>

      </>
    );
  }
}

export default FullProduct;
