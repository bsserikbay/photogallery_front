import React from "react";
import {Route, Switch} from "react-router-dom";
import Gallery from "./containers/Gallery/Gallery";
import AddPhoto from "./containers/AddPhoto/AddPhoto";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {Redirect} from "react-router";
import UserGallery from "./containers/UserGallery/UserGallery";

const ProtectedRoute = (props) => {
  return props.isAllowed ? <Route {...props} /> : <Redirect to={props.redirectTo} />;
};

const Routes = props => {
  return (
    <Switch>
      <Route path="/" exact component={Gallery} />
      <ProtectedRoute
        path="/gallery/new"
        exact
        component={AddPhoto}
        isAllowed={props.user}
        redirectTo="/login"
      />
      <Route path="/gallery/:id" exact component={UserGallery} />
      <Route path="/registration" exact component={Register} />
      <ProtectedRoute
        path="/login"
        exact
        component={Login}
        isAllowed={!props.user}
        redirectTo="/"
      />
    </Switch>
  );
};

export default Routes;
