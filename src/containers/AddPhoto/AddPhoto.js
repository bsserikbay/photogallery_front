import React, {Component} from "react";
import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {createPhoto} from "../../store/actions/photosActions";
import {connect} from "react-redux";

class AddPhoto extends Component {

  createNewProduct = product => {
    this.props.onPhotoCreated(product).then(() => {
      this.props.history.push("/");
    });
  };



  render() {
    return (
      <>
        <h1>Add new photo</h1>
        <PhotoForm onSubmit={this.createNewProduct} />
      </>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onPhotoCreated: photo => dispatch(createPhoto(photo))
  }
};

export default connect(null, mapDispatchToProps)(AddPhoto);
