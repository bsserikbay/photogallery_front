import React, {Component} from "react";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
  state = {
    username: "",
    password: ""
  };
  inputChangeHandler = e => {
    this.setState({[e.target.name]: e.target.value});
  };
  formSubmitHandler = e => {
    e.preventDefault();
    this.props.registerUser(this.state, this.props.history);
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors &&
      this.props.error.errors[fieldName] &&
      this.props.error.errors[fieldName].properties &&
      this.props.error.errors[fieldName].properties.message;
  };

  render() {
    return (
      <>
        <h2>Register new user</h2>
        <Form onSubmit={this.formSubmitHandler}>
          <FormElement
            type="text"
            title="Username"
            name="username"
            required={false}
            placeholder="Username"
            error={this.getFieldError("username")}
            value={this.state.username}
            onChange={this.inputChangeHandler}
          />
          <FormElement
            type="password"
            title="Password"
            name="password"
            required={false}
            placeholder="Password"
            error={this.getFieldError("password")}
            value={this.state.password}
            onChange={this.inputChangeHandler}
          />
          <FormGroup row>

            <Col sm={{offset: 2, size: 1}}>
              <Button type="submit" color="primary">
                Register
              </Button>
            </Col>
            <Col sm={{size: 3}}>
              <FacebookLogin />
            </Col>


          </FormGroup>
        </Form>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.users.registerError
  };
};
const mapDispatchToProps = dispatch => {
  return {
    registerUser: (userData, history) => dispatch(registerUser(userData, history))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
